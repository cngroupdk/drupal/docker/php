FROM php:7.4-fpm

COPY www.conf /usr/local/etc/php-fpm.d/www.conf
COPY php.ini /usr/local/etc/php/php.ini

WORKDIR /var/www/html

USER root
RUN apt-get update \
   && apt-get install -y \
        libldap2-dev libssl1.0 libssl-dev libfontconfig  libfreetype6-dev libxrender1 \
        libzip-dev libbz2-dev libxslt-dev libpng-dev libjpeg-dev libmcrypt-dev libpq-dev libc-client-dev libkrb5-dev libonig-dev \
        default-mysql-client libmemcached11 libmemcachedutil2 libmemcached-dev \
        patch wget git git-core vim mc gettext-base  \
   && rm -rf /var/lib/apt/lists/* \
   && docker-php-ext-configure gd --with-jpeg --with-freetype \
   && docker-php-ext-configure ldap --with-libdir=lib/x86_64-linux-gnu \
   && docker-php-ext-install bcmath gd ldap mbstring mysqli opcache pdo pdo_mysql pdo_pgsql zip xsl bz2 exif \
   && pecl install memcached-3.1.5 \
   && docker-php-ext-enable memcached \
   && pecl install apcu \
   && docker-php-ext-enable apcu \
   && curl -fSL "https://github.com/drush-ops/drush-launcher/releases/download/0.9.1/drush.phar" -o /usr/local/bin/drush \
   && chmod +x /usr/local/bin/drush \
   && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
